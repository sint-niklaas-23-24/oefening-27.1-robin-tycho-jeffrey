﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Oefening_23._3
{
    internal class Werknemer
    {
        private string _naam;
        private string _voornaam;
        private decimal _loon;
        private BitmapImage _geslacht;

        public Werknemer() 
        { 

        }
        public Werknemer(string naam, string voornaam, decimal loon, BitmapImage geslacht)
        {
            Naam = naam;
            Voornaam = voornaam;
            Loon = loon;
            Geslacht = geslacht;
        }
        public BitmapImage Geslacht
        {
            get { return _geslacht; }
            set { _geslacht = value; }
        }
        public string Gegevens
        {
            get { return this.ToString(); }
        } 
        public string Naam 
        { 
            get { return _naam; } 
            set 
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("De achternaam werd niet ingevuld!");
                }
                else if (value.Any(char.IsDigit))
                {
                    throw new Exception("De achternaam mag geen cijfer bevatten!");
                }
                else
                {
                    _naam = value;
                }
            }
        }
        public string Voornaam
        {
            get { return _voornaam; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("De voornaam werd niet ingevuld!");
                }
                else if (value.Any(char.IsDigit))
                {
                    throw new Exception("De voornaam mag geen cijfer bevatten!");
                }
                else
                {
                    _voornaam = value;
                }
            }
        }
        public decimal Loon
        {
            get { return _loon; }
            set 
            {
                if (value > 0)
                {
                    _loon = value;
                }
                else
                {
                    throw new Exception("De loon kan niet kleiner zijn dan 0!");
                }
            }
        }
        public virtual decimal Verdiensten()
        {
            return Loon;
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Werknemer r = (Werknemer)obj;
                    if (this.Naam == r.Naam && this.Voornaam == r.Voornaam)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {
            return $"{this.GetHashCode()}| {Naam.PadRight(20)} {Voornaam.PadRight(20)} (\u20AC {Verdiensten()})".PadRight(20);
        }
    }
}
