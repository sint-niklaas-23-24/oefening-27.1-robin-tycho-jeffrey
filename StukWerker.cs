﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Oefening_23._3
{
    internal class StukWerker : Werknemer
    {
        private int _aantal;

        public StukWerker(string naam, string voornaam, decimal loonPerStuk, BitmapImage geslacht, int aantal) : base(naam, voornaam, loonPerStuk, geslacht)
        {
            Aantal = aantal;
        }
        public int Aantal 
        { 
            get { return _aantal; } 
            set 
            { 
                if (value > 0)
                {
                    _aantal = value;
                }
                else
                {
                    throw new Exception("Het aantal kan niet kleiner of gelijk aan 0 zijn!");
                }
            } 
        }
        public override decimal Verdiensten()
        {
            return base.Verdiensten() * Aantal;
        }
        public override string ToString()
        {
            return base.ToString() + " - StukWerker"; 
        }
    }
}
