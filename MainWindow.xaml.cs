﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Oefening_23._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Werknemer> werknemers = new List<Werknemer>();

        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            decimal loon;
            int aantal;
            decimal commissie;
            Werknemer werknemer;

            try
            {
                if (decimal.TryParse(txtLoon.Text, out loon))
                {
                    if (rdoWerknemer.IsChecked == true)
                    {
                        werknemer = new Werknemer(txtAchternaam.Text, txtVoornaam.Text, loon, GeslachtCheck());
                        VoegWerknemerToe(werknemer);
                    }
                    else if (rdoStukWerker.IsChecked == true)
                    {
                        if (int.TryParse(txtAantal.Text, out aantal))
                        {
                            werknemer = new StukWerker(txtAchternaam.Text, txtVoornaam.Text, loon, GeslachtCheck(), aantal);
                            VoegWerknemerToe(werknemer);
                        }
                        else
                        {
                            throw new FormatException("Het aantal moet een geheel getal zijn!");
                        }
                    }
                    else if (rdoUurWerker.IsChecked == true)
                    {
                        if (int.TryParse((txtAantal.Text), out aantal))
                        {
                            werknemer = new UurWerker(txtAchternaam.Text, txtVoornaam.Text, loon, GeslachtCheck(), aantal);
                            VoegWerknemerToe(werknemer);
                        }
                        else
                        {
                            throw new FormatException("Het aantal moet een geheel getal zijn!");
                        }
                    }
                    else if (rdoComissieWerker.IsChecked == true)
                    {
                        if (int.TryParse((txtAantal.Text), out aantal))
                        {
                            if (decimal.TryParse((txtCommissie.Text).Replace(".", ","), out commissie))
                            {
                                werknemer = new CommissieWerker(txtAchternaam.Text, txtVoornaam.Text, loon, GeslachtCheck(), commissie, aantal);
                                VoegWerknemerToe(werknemer);
                            }
                            else
                            {
                                throw new FormatException("De commissie moet een decimaal getal zijn!");
                            }
                        }
                        else
                        {
                            throw new FormatException("Het aantal moet een geheel getal zijn!");
                        }
                    }
                }
                else
                {
                    throw new FormatException("Het loon moet een decimaal getal zijn!");
                }
                LeegTekstvakken();
                RefreshListBox();
            }
            catch (FormatException fex)
            {
                MessageBox.Show(fex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void RefreshListBox()
        {
            lbInhoud.ItemsSource = null;
            lbInhoud.ItemsSource = werknemers;
        }
        private void rdoComissieWerker_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.Visibility = Visibility.Visible;
            txtAantal.Visibility = Visibility.Visible;
            lblCommissie.Visibility = Visibility.Visible;
            txtCommissie.Visibility = Visibility.Visible;
        }
        private void rdoStukWerker_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.Visibility = Visibility.Visible;
            txtAantal.Visibility = Visibility.Visible;
            lblCommissie.Visibility = Visibility.Hidden;
            txtCommissie.Visibility = Visibility.Hidden;
        }
        private void rdoUurWerker_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.Visibility = Visibility.Visible;
            txtAantal.Visibility = Visibility.Visible;
            lblCommissie.Visibility = Visibility.Hidden;
            txtCommissie.Visibility = Visibility.Hidden;
        }
        private void Werknemer_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.Visibility = Visibility.Hidden;
            txtAantal.Visibility = Visibility.Hidden;
            lblCommissie.Visibility = Visibility.Hidden;
            txtCommissie.Visibility = Visibility.Hidden;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            rdoWerknemer.IsChecked = true;
            lblAantal.Visibility = Visibility.Hidden;
            txtAantal.Visibility = Visibility.Hidden;
            lblCommissie.Visibility = Visibility.Hidden;
            txtCommissie.Visibility = Visibility.Hidden;
        }
        private void VoegWerknemerToe(object werknemer)
        {
            if (werknemers.Contains((Werknemer)werknemer))
            {
                throw new Exception("Deze werknemer bestaat al!");
            }
            else
            {
                werknemers.Add((Werknemer)werknemer);
            }
        }
        private BitmapImage GeslachtCheck()
        {
            if (rdoMan.IsChecked == true)
            {
                return new BitmapImage(new Uri(@"Images/Man.png", UriKind.Relative));
            }
            else if (rdoVrouw.IsChecked == true)
            {
                return new BitmapImage(new Uri(@"Images/Vrouw.jpg", UriKind.Relative));
            }
            else
            {
                throw new Exception("Er werd geen geslacht aangeduid!");
            }
        }
        private void LeegTekstvakken()
        {
            txtAantal.Text = string.Empty;
            txtAchternaam.Text = string.Empty;
            txtVoornaam.Text = string.Empty;
            txtCommissie.Text = string.Empty;
            txtLoon.Text = string.Empty;
        }
    }
}
