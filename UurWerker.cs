﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Oefening_23._3
{
    internal class UurWerker : Werknemer
    {
        private double _uren;

        public UurWerker(string naam, string voornaam, decimal loon, BitmapImage geslacht, double aantalUren) : base(naam, voornaam, loon, geslacht)
        {
            Uren = aantalUren;
        }
        public double Uren 
        {  
            get { return _uren; } 
            set 
            {  
                if (value > 0)
                {
                    _uren = value;
                }
                else
                {
                    throw new Exception("Het aantal uren kan niet kleiner of gelijk zijn aan 0!");
                }
            } 
        }
        public override decimal Verdiensten()
        {
            return base.Verdiensten() * Convert.ToDecimal(Uren);
        }
        public override string ToString() 
        { 
            return base.ToString() + " - UurWerker";
        }
    }
}
